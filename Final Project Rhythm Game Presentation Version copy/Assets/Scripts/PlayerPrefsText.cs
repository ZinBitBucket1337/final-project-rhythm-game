﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPrefsText : MonoBehaviour
{
    // A public string called Name, this is to display the grading system like the score.
    public string Name;

    // Update is called once per frame
    void Update()
    {
        // Attaching to the text component and equalling it to the Name variable for the integer of the PlayerPrefs.
        GetComponent<Text>().text = PlayerPrefs.GetInt(Name) + "";
    }
}
