﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    // Attachment to the Rigidbody2D with a variable represented by rb.
    Rigidbody2D rb;
    // A public float for the speed of the sound note (DESIGNER CAN MODIFY IN INSPECTOR).
    public float speed;
    // A boolean value that is set to false by default, help's with synchornizing the notes to the music if played again.
    bool called = false;

    //
    void Awake()
    {
        // The variable for Rigidbody2D is attaching itself to the Rigidbody2D component.
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // An if statement for the notes for every new start of the game.
        if (PlayerPrefs.GetInt("Start")==1&&!called)
        {
            // The speed movement of the sound notes, going at the negative y direction.
            rb.velocity = new Vector2(0, -speed);
            // Boolean value is set to true.
            called = true;
        }
    }
}
