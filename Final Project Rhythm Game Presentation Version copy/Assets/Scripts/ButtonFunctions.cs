﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonFunctions : MonoBehaviour
{
    // A public button function for loading the scene
    public void LoadScene(int a)
    {
        // Call's to Unity's SceneManager to transition to Scene int a.
        SceneManager.LoadScene(a);
    }

    // A public button function for quiting out of the game
    public void Quit()
    {
        // Quit.
        Application.Quit();
    }
}
