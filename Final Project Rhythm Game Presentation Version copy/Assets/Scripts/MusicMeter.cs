﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicMeter : MonoBehaviour
{
    // The float value of the MusicMeter, represented by the variable mm.
    float mm;
    // Attchment to the game object called variable needle.
    GameObject needle;

    // Start is called before the first frame update
    void Start()
    {
        // The neelde varaible will attch itself to the game object labeled needle.
        needle = transform.Find("needle").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        // The MusicMeter variable is equal to the PlayerPrefs integer of the MusicMeter.
        mm = PlayerPrefs.GetInt("MusicMeter");
        // This is the math formula for the positon movement of the needle.
        needle.transform.localPosition = new Vector3((mm - 25) / 16.666f, 0, 0);
    }
}
