﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    // A bool variable named called and should by default be false.
    bool called = false;

    // Update is called once per frame
    void Update()
    {
        // An if statement for the notes for every new start of the game.
        if (PlayerPrefs.GetInt("Start")==1&&!called)
        {
            // Attching to the AudioSource Component and controlling the ability of when to play the audio source.
            GetComponent<AudioSource>().Play();
            // Boolean value is set to true.
            called = true;
        }
    }
}
