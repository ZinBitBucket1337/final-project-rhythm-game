﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFrame : MonoBehaviour
{
    // Attachment for the Sprite Renderer with a variable represented by sr.
    SpriteRenderer sr;
    // Attachment for the KeyCode with a variable represented by key (DESIGNER CAN MODIFY EACH KEY IN INSPECTOR).
    public KeyCode key;
    // A boolean variable called active which is set to false by defualt.
    bool active = false;
    // Attachment to the GameObjects with the variables represented by note and gM (GameManager).
    GameObject note, gM;
    // A color variable for each sound frame, turns into back to it's original color.
    Color old;
    // A public boolean variable for activating createMode, helpful tool to place sound notes.
    public bool createMode;
    // A public game object attachment to the variable n.
    public GameObject n;
    //

    //
    void Awake()
    {
        // The variable for the sprite renderer is attaching itself to the sprite renderer component.
        sr = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        // Finding and placing the GameManager under the variable gM.
        gM = GameObject.Find("GameManager");
        // Setting the color of each sound frame to it's default color.
        old = sr.color;
        // Recalling the ResetStreak Function from the GameManager component.
        gM.GetComponent<GameManager>().ResetStreak();
    }

    // Update is called once per frame
    void Update()
    {
        // An if statement for the process behing createMode.
        if (createMode)
        {
            // Anf if statement that will execute an instantiate when the KeyCode is pressed.
            if (Input.GetKeyDown(key))
            {
                // Clones the sound notes.
                Instantiate(n, transform.position, Quaternion.identity);
            }
        }
        // An else statement for whenever createMode is not activated in the inspector.
        else
        {
            // An if statement that will start the method StartCoroutine with the IEnumerator Pressed.
            if (Input.GetKeyDown(key))
            {
                // The StartCorutine for the IEnumerator Pressed.
                StartCoroutine(Pressed());
            }

            // An if statement for the streak system.
            if (Input.GetKeyDown(key) && active)
            {
                // Destroy's the music note
                Destroy(note);
                // Recalling the AddStreak Function from the GameManager component.
                gM.GetComponent<GameManager>().AddStreak();
                // Function call to AddScore.
                AddScore();
                // Set's the active to false.
                active = false;
            }
            else if(Input.GetKeyDown(key) && !active)
            {
                //gM.GetComponent<GameManager>().ResetStreak();
            }

        }
    }

    // A OnTriggerEnter2D collsion function for the sound frames to destroy the notes.
    void OnTriggerEnter2D(Collider2D col)
    {
        // An if statment for when the object with the FinalNote tag collides with a sound frame.
        if (col.gameObject.tag == "FinalNote")
        {
            // Recalling the Victory Function from the GameManager component.
            gM.GetComponent<GameManager>().Victory(); 
        }

        // An if statement that will process how the notes and sound frames collide.
        if (col.gameObject.tag == "Note")
        {
            // The note object enables collison with the sound frame.
            note = col.gameObject;
            // Set active to false.
            active = true;
        }
    }

    // A OnTriggerExit2D collsion
    void OnTriggerExit2D(Collider2D col)
    {
        // Set active to false.
        active = false;
        //gM.GetComponent<GameManager>().ResetStreak();
    }

    // The AddScore Function definition.
    void AddScore()
    {
        // The scoring system, that is recalling th function GetScore from the GameManager.
        PlayerPrefs.SetInt("Score", PlayerPrefs.GetInt("Score") + gM.GetComponent<GameManager>().GetScore());
    }

    // The IEnumerator for when the keycode is pressed for the sound frame.
    IEnumerator Pressed()
    {
        // Turn the color of the sound frame black.
        sr.color = new Color(0, 0, 0);
        // Set it to the color blakc for 0.05 seconds.
        yield return new WaitForSeconds(0.05f);
        // Set the sound frame to it's default color.
        sr.color = old;
    }
}
