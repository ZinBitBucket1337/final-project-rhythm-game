﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // The int variable for the multiplier, should by defualt be at 1.
    int multiplier = 1;
    // The int variable for the streak, should by default be at 0.
    int streak = 0;

    // Start is called before the first frame update
    void Start()
    {
        // When the game is starting(again), set the score back to the default of zero.
        PlayerPrefs.SetInt("Score", 0);
        // When the game is starting(again), set the MusicMeter back to the default of 25.
        PlayerPrefs.SetInt("MusicMeter", 25);
        // When the game is starting(again), set the Streak back to the default of zero.
        PlayerPrefs.SetInt("Streak", 0);
        // When the game is starting(again), set the Highest Streak back to the default of zero.
        PlayerPrefs.SetInt("HighStreak", 0);
        // When the game is starting(again), set the multiplier back to the default of zero.
        PlayerPrefs.SetInt("Mult", 1);
        // When the game is starting(again), set the number of notes hit back to the default of zero.
        PlayerPrefs.SetInt("NotesHit", 0);
        // When the game is starting(again), set the music and notes to it's default rhthym.
        PlayerPrefs.SetInt("Start", 1);
    }

    //


    // A OnTriggerEnter2D collision function.
    void OnTriggerEnter2D(Collider2D col)
    {
        // Attaching the the GameManager component and recalling the ResetStreak function.
        GetComponent<GameManager>().ResetStreak();
    }

    // The public function definition for AddStreak.
    public void AddStreak()
    {
        // The if statement for how the MusicMeter position will be affected if player hit's a note.
        if(PlayerPrefs.GetInt("MusicMeter") + 1<50)
            PlayerPrefs.SetInt("MusicMeter", PlayerPrefs.GetInt("MusicMeter") + 1);
        // Increment the streak by 1 everytime.
        streak++;
        // All these if, if else, and else statements states that define how much of a steak is equal to a multiplier.
        if (streak >= 15)
        {
            multiplier = 4;
        }
        else if (streak >= 10)
        {
            multiplier = 3;
        }
        else if (streak >= 5)
        {
            multiplier = 2;
        }
        else
            multiplier = 1;
        // An if statement that will display the highest streak value.
        if (streak > PlayerPrefs.GetInt("HighStreak"))
        {
            // Define the HighStreak and the variable.
            PlayerPrefs.SetInt("HighStreak", streak);
        }
        // Display the number of notes hit.
        PlayerPrefs.SetInt("NotesHit", PlayerPrefs.GetInt("NotesHit") + 1); 
        // UpdateGUI function call.
        UpdateGUI();
    }

    // A public ResetStreak function definition.
    public  void ResetStreak()
    {
        // If a player misses a note decrement by two on the MusicMeter.
        PlayerPrefs.SetInt("MusicMeter", PlayerPrefs.GetInt("MusicMeter") - 2);
        if (PlayerPrefs.GetInt("MusicMeter") < 0)
            // If the player reach's zero in the MusicMeter then they will lose
            Lose();
        // streak variable value set back to zero.
        streak = 0;
        // multiplier variable value set back to zero.
        multiplier = 1;
        // UpdateGUI function call.
        UpdateGUI();
    }

    // Function definition for Lose.
    void Lose()
    {
        // Music and notes reset back to there defualt pace.
        PlayerPrefs.SetInt("Start", 0);
        // Transition to the lost screen.
        SceneManager.LoadScene(3);
    }

    // Function definiton for victory
    public void Victory()
    {
        // Music and notes reset back to there defualt pace.
        PlayerPrefs.SetInt("Start", 0);
        // An if statement on how to display the high score and replace the high score value.
        if (PlayerPrefs.GetInt("HighScore")< PlayerPrefs.GetInt("Score"))
        {
            PlayerPrefs.SetInt("HighScore", PlayerPrefs.GetInt("Score"));
        }
        // Transition to the victory screen.
        SceneManager.LoadScene(2);
    }

    // Function definition for UpdateGUI.
    public void UpdateGUI()
    {
        // Display the increase or dcrease of the streak in-game.
        PlayerPrefs.SetInt("Streak", streak);
        // Display the increase or dcrease of the multiplier in-game.
        PlayerPrefs.SetInt("Mult", multiplier);
    }

    // The public function definition of the GetScore, points system (DESIGNER CAN MODIFY THE NUMBER 100).
    public int GetScore()
    {
        // Every music note is worth a 100 points times the value of the multiplier.
        return 100 * multiplier;
    }
}
